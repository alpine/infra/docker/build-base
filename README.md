# Build base

This is a docker image that can be used to build Alpine Linux packages with
abuild. Currently, only an image based on `edge` is provided. To build packages
for another version, you could change the version in `/etc/apk/repositories` and
run `apk upgrade -U --available`.

## Preparation

Before you can build packages, you need to generate a signing key. This can be
done by running:

```sh
abuild-keygen -ai
```

Also make sure that the correct repositories are enabled in
`/etc/apk/repositories.`

## Images

* `alpinelinux/build-base:latest`
