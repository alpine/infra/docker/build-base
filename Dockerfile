ARG VERSION=edge
FROM registry.alpinelinux.org/img/alpine:$VERSION

COPY overlay/ /

RUN setup.sh

USER buildozer
